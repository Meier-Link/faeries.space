#! /bin/bash

if [ $# -ne 1 ]
then
    echo "Project dir must be provided"
    exit 1
fi

test_project=
if [ -d $1 ]
then
    test_project=$1
else
    echo "Given project dir doesn't exist (given: $1)"
    exit 1
fi
