#! /bin/bash

if [ $# -ne 2 ]
then
    echo "Both branch and project dir must be provided"
    exit 1
fi

version=
build_name=develop
if echo "$1" | grep 'release-[0-9]*\.[0-9]*' > /dev/null
then
    version="$(echo $1 | cut -d'-' -f2)"
    build_name="release version ${version}"
else if echo "$1" | grep 'release/[0-9]*\.[0-9]*' > /dev/null
then
    version="$(echo $1 | cut -d'/' -f2)"
    build_name="release candidate of ${version}"
fi
fi

build_project=
if [ -d $2 ]
then
    build_project=$2
else
    echo "second argument must be the project directory (given: $1)"
    exit 1
fi

echo "Build $build_name for $build_project."
