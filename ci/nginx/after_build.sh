#! /bin/bash

DIRNAME=${CI_PROJECT_DIR:=/opt/faeries.space}

if [ -d $DIRNAME ]
then
    rm -rf $DIRNAME/ci/nginx/context/
fi
