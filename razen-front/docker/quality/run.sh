#!/bin/bash

echo "Run eslint on $1/js/..."
eslint $1/js/*.js
if [ $? -ne 0 ]; then exit 1; fi
echo "Run HTML Validate..."
html-validate $1/index.html
if [ $? -ne 0 ]; then exit 1; fi
