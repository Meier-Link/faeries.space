/**
 * Simplified Redux implementation for the needs of the project's start.
 *
 * NB: useful for study purpose, but give a try to the official Redux: redux.js.org
 */
'use strict';

function createStore(initState)
{
    let state = initState || {};

    function _hasKey(key)
    {
        return Object.prototype.hasOwnProperty.call(state, key);
    }

    function _reducer(key, val)
    {
        if (_hasKey(key))
        {
            for (let follower of state[key].followers)
            {
                // TODO(JBR) How to make sure a follower fail doesn't break update
                // of the key?
                follower(key, state[key].payload, val);
            }
            state[key].payload = val;
        }
    }

    function getState()
    {
        return state;
    }

    /**
     * Make possible to register new items in the state
     */
    function register(ext)
    {
        for (let key of ext)
        {
            if (!_hasKey(key))
            {
                state[key] = {followers: [], payload: null};
            }
        }
    }

    /**
     * Subscribe to the changes on a given key
     */
    function subscribe(keyVal, callback)
    {
        // TODO(JBR) deal with key which doesn't yet exist
        //          Make sure callback 'is' a callback.
        if (_hasKey(keyVal))
        {
            state[keyVal].followers.push(callback);
        }
    }

    function dispatch(action)
    {
        _reducer(action.key, action.value);
    }

    return { getState, register, subscribe, dispatch };
}

/* Example */
const exampleStore = createStore();
exampleStore.register(['dark-mode', 'username']);
exampleStore.subscribe(
    'dark-mode',
    (key, oldVal, newVal) => console.log('Update the class to ' + newVal));
exampleStore.dispatch({key: 'dark-mode', value: 'enabled'});

const store = createStore();

let redux = {store: store, createStore: createStore};
export { redux };
