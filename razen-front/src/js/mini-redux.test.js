/**
 * mini-redux.test.js
 */

import { redux } from './mini-redux.js';

test('stores an retreive dark-mode boolean value', () => {
    const store = redux.createStore();
    store.register(['dark-mode']);
    let darkModeEnabled = true;
    store.subscribe(
        'dark-mode',
        (key, oldVal, newVal) => {
            darkModeEnabled = newVal;
        });
    store.dispatch({key: 'dark-mode', value: false});
    let state = store.getState();
    expect(Object.keys(state).length).toBe(1);
    expect(state['dark-mode'].payload).toBe(false);
    expect(darkModeEnabled).toBe(false);
});
