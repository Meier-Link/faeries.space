The Faeries Space main repository
=================================

Currently, this repository holds all the sources of the Faeries Space project.

## Requirements

**TBD**

## Building

**TBD**

## Installation

**TBD**

## Running

**TBD**

## Tests

**TBD**

## Roadmap

### Current Step: Step 0

Setup the very basics of the project.

An instance will host the Gitlab runner with the RC (tests) and release (deployment) jobs.
The deployment will consist in scp-ing to the web server (or using a registry?).

At this step, everything will be on a single instance in the Cloud:
* Nginx for web requests
* Main website (i.e. faeries.space)
* The Universe website (i.e. razen.faeries.space)
* The websites frontend *and* backend.

| Task Description          | Status   | Notes                                                                          |
|---------------------------|----------|--------------------------------------------------------------------------------|
| The Runner instance       | Done     | This is the next step to do.                                                   |
| -> Create / setup instance | Done    | instance, volume, docker, etc.                                                 |
| -> Gitlab Runnner         | Done     | The runner is up and running. Further comprehension will come along the needs. |
| -> Docker registry        | Done     | Deployed a registry on the same server as the runner in order to be able to store built images with gitlab-ci locally. |
| The Host instance         | On going | Attempted to reconvert an existing one - Finally create a new one from scratch |
| -> Volume                 | Done     | Let's learn the best way to use it.                                            |
| -> Create / setup instance | Done    | Link, then mount volume and prepare subdirs for Docker & co.                   |
| -> Link between ci and pr | Todo     | Make possible to use Private IP from CI instance to Pr one (something to learn, here) |
| -> Install Docker         | Done     |                                                                                |
| -> Install Nginx          | Won't Do | The Nginx will be managed through the container which also hold the fronts sources. |
| -> Update DNS             | Todo     | Update on DNS provider to the IP of the new instance.                          |
| Gitlab Runner             | On going | Created the Gitlab CI file, but need the VM to run on.                         |
| -> gitlab-ci.yml          | On going | The file is created, Now have to fill the build, test and deploy scripts.      |
| -> Reach the runner       | Done     | Need the runner to be deployed and configured on the instance.                 |
| -> Docker in Runner       | Done     | Here we need to create containers from the runnner's container for project's needs.|
| -> build step             | Done     | cf. Docker in runner, but in practice.                                         |
| -> lint step              | Done     | Only on the front (js, and html/css) for now.                                  |
| -> test step              | Done     | Write test to be executed (cf. the mini-redux for the example)                 |
| -> deploy step            | Todo     | cf. the Nota Bene below - Also need the Host instance                          |
| (Game) Landing Page       | On going | It remains a "TODO" for the `main`. Hope to put a map of the universe.         |
| -> Connect to Universe    | Todo     | Connect to the Universe repo, which have to be updated.                        |
| -> Render SVG map         | Todo     | At this step, just make possible to render the SVG (front architecture for later) |
| (Main) Landing Page       | Todo     | At first, it'll be a blog with steps details.                                  |
| -> Render MD files        | Todo     | Look for simple projects to render markdown files.                             |

**NB:**
* Currently, the artifacts are stored on the gitlab host only. Now, we'll look to push the docker images to a common registry.

When the choice is implemented, write tests on our mini-redux as a proof of concept.

### Previous Step:

None

### Next Step: TBD

Will include websites' backend (webserver for REST API and the dispatcher project for the Universe website).

First step on chosing / deploying a database (and make possible to any visitor to subscribe to a future newsletter?).

One step further in the Main website - reach a clean state with articles from the writings' project and use of the
Gitlab API to get the repo content and structure.

First step in mechanics concept for the maps (imply to update / create maps in the universe's repo, then use the same
idea as for the Main website to get maps). **NB:** something more sophisticated is scheduled for later steps, with
Gitlab CI.
