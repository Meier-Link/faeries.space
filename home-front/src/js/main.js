/**
 * main.js
 */

import { state } from '../mechanics/libs/state-store.js';
import { route } from '../mechanics/libs/router.js';

state.store.register(['nav-menu']);
state.store.subscribe(
    'nav-menu',
    (key, oldVal, newVal) => {
        let valStr = '';
        newVal.forEach(x => valStr += '\n' + x.href + ' -> ' + x.text);
        console.log(`Loaded new menu: ${valStr}`);
    }
);
state.store.dispatch({
    key: 'nav-menu',
    value: [
        { href: '#/home',       text: 'Home' },
        { href: '#/about',      text: 'About' },
        { href: '#/blog',       text: 'Blog' },
        { href: '#/licenses',   text: 'Licenses' }
    ]
});

route.trigger((curVal) => {
    console.log(`Moved to ${curVal}`);
    document.querySelectorAll('section.top-level').forEach((s => s.style.display = 'none'));
    document.querySelector(`section.top-level[role=${curVal}]`).style.display = 'block';
});
