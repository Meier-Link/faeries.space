/**
 * get current value of the url and give a state to be followed from other
 * componenents in the web page in order to modify it.
 */

import { state } from './state-store.js';

if (-1 == window.location.href.indexOf('#'))
{
    window.location.href += '#/home';
}
let loc = window.location.href;
let currentRoute = loc.substr(loc.indexOf('#') + 2);

const routeState = state.createStore({route: currentRoute});
const updateRoute = (newRoute) => {
    const newVal = newRoute.substr(newRoute.indexOf('#') + 2);
    routeState.dispatch({key: 'route', value: newVal});
    let loc = window.location.href;
    loc = loc.substr(0, loc.indexOf('#') + 2);
    window.location.href = loc + newRoute;
};
/**
 * Build a link.
 * @param href {String} the Link where it sends.
 * @param text {String} the text of the link.
 * @param style {String} either light or dark (default: light)
 */
const buildLink = (href, text, style='light') => {
    let link = document.createElement('a');
    link.classList.add(style);
    link.href = href;
    // TODO(JBR) we should protect against injection here.
    link.innerHTML = text;
    link.addEventListener("click", () => {
        updateRoute(href);
    });
    return link;
};
const subscrite = (callback) => {
    return routeState.trigger('route', callback);
};
const route = {
    get: routeState.getState,
    trigger: (callback) => routeState.trigger('route', callback),
    update: updateRoute,
    buildLink: buildLink,
    buildButton: (href, text, style='light') => {
        let link = buildLink(href, text, style);
        // TODO(JBR) class to test and improve.
        link.classList.add('button');
        return link;
    }
};

export { route };
