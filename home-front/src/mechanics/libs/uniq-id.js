/**
 * Unique ID Generator.
 */

/**
 * @brief Unique ID generator.
 * @input {List[int]} Format of the uid generated. Each int represents a string
 *  with 4 base16 digits (default: [2, 1, 1, 3] to generate UUIDv4 like unique id).
 * @return {String} a unique id with bloc of base16 digits spearated by '-'.
 */
function uid(format=[2, 1, 1, 3])
{
    let fullLength = 0;
    format.forEach(x => fullLength += x);

    let uniqId = [];
    let cumul = 0;
    let result = "";
    let values = window.crypto.getRandomValues(new Uint16Array(fullLength))
    values.forEach(x => uniqId.push(Number(x).toString(16)));
    format.forEach(x => {
        if (0 != cumul) result += '-';
        result += uniqId.slice(cumul, cumul + x).join('');
        cumul += x;
    });
    return result;
}

export { uid };
