/**
 * state-store.test.js
 */

import { state } from './state-store.js';

test('stores an retreive dark-mode boolean value', () => {
    const store = state.createStore();
    store.register(['dark-mode']);
    let darkModeEnabled = true;
    store.subscribe(
        'dark-mode',
        (key, oldVal, newVal) => {
            darkModeEnabled = newVal;
        });
    store.dispatch({key: 'dark-mode', value: false});
    let states = store.getState();
    expect(Object.keys(states).length).toBe(1);
    expect(states['dark-mode']).toBe(false);
    expect(darkModeEnabled).toBe(false);
});
