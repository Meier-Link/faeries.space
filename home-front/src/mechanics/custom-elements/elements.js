/**
 * elements.js
 * @brief Catalog of registered custom elements.
 */

import { FdsBasicExample } from './fds-basic-example/main.js';
import { FdsGitlabTree } from './fds-gitlab-tree/main.js';
import { FdsToggleMenu } from './fds-toggle-menu/main.js';

// TODO(JBR) the attribute name may be a static attribute of the CustomElement class.
window.customElements.define('fds-basic-example', FdsBasicExample);
window.customElements.define(FdsGitlabTree.tagName, FdsGitlabTree);
window.customElements.define('fds-toggle-menu', FdsToggleMenu);
