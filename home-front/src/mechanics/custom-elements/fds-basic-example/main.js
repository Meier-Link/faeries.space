/**
 * FdsBasicExample
 * @brief a simple example of custom element which serves as a model for others.
 */

import { fetchStaticAsString } from '/backend/main.js';


class FdsBasicExample extends HTMLElement
{
    /* default attributes with default value. */
    // TODO(JBR) Here, the value should be a function which'll used to
    // use a default value if the given one is undefined or invalid.
    static defaultAttributes = {
        'target-date': new Date()
    };

    /**
     * Load the innerHTML of the custom element from backend.
     * @return the value to set on the innerHTML.
     */
    async loadInnerHTML()
    {
        let remoteBaseDir = 'mechanics/custom-elements/fds-basic-example';
        let cssData = await fetchStaticAsString(`${remoteBaseDir}/style.css`);
        let htmlData = await fetchStaticAsString(`${remoteBaseDir}/index.html`);
        let data = `<style type="text/css" scoped>
    ${cssData}
</style>
${htmlData}`;
        return data;
    }

    /**
     * Apply the attributes to the innerHTML value.
     * The attribute name must be surrounded by {{}} in the HTML template.
     */
    renderTemplate()
    {
        Object.keys(this.attributes).forEach((attr) => {
            if (this.template) {
                this.innerHTML = this.template.replace(`{{${attr}}}`, this.attributes[attr]);
            }
        });
    }

    /**
     * Method used by the browser to get the list of attributes to observe from
     * the HTML.
     */
    static get observedAttributes() {
        return Object.keys(FdsBasicExample.defaultAttributes);
    }

    /**
     * Method called whenevere the custom element is instanciated.
     */
    connectedCallback()
    {
        Object.keys(FdsBasicExample.defaultAttributes).forEach((attr) => {
            // TODO(JBR) To deal with possible errors, use a callback in the
            // FdsBasicExample.defaultAttributes associated values.
            this.attributes[attr] = this.getAttribute(attr);
            this.renderTemplate();
        });
        /* // Kept under the pillow to be used when the attributes setter is set.
        let targetDate = this.getAttribute('target-date');
        targetDate = new Date(targetDate);
        if (!targetDate || 'Invalid Date' == targetDate.toString())
        {
            // Seems strange, but new Date(null) returns a date, which's unix 0.
            targetDate = new Date();
        }
        this.targetDate = targetDate;
        */
    }

    /**
     * Apply changes whenever the attribute attr is updated.
     * @param attr {String} the attribute which has changed.
     * @param oldValue {String} the old value.
     * @param newValue {String} the new value.
     */
    attributeChangedCallback(attr, oldValue, newValue)
    {
        this.attributes[attr] = newValue;
        this.renderTemplate();
    }

    constructor()
    {
        super();  // NB: Never before this line, which is absoluetly mandatory...
        // NB: Here we'll have a call to the setter from the static default
        // attributes.
        Object.defineProperty(FdsBasicExample, 'attributes', {
            /* This is a little trick required to get the attribute writable.
             * NB: in fact, maybe we can make it non writable?
             */
            writable: true,
            value: {...FdsBasicExample.defaultAttributes}
        });
        Object.defineProperty(FdsBasicExample, 'template', {
            writable: true,
            value: ''
        });
        this.loadInnerHTML().then((data) => {
            this.template = data;
            this.renderTemplate();
        });
    }
}

export { FdsBasicExample };
