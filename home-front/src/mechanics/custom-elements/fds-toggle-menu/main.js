/**
 * FdsToggleMenu
 * The Toggle Menu element
 */


import { fetchStaticAsString } from '/backend/main.js';
import { state } from '/mechanics/libs/state-store.js';
import { route } from '/mechanics/libs/router.js';


class FdsToggleMenu extends HTMLElement {
    /** Specific to FdsToggleMenu **/

    /**
     * Build the dynamic part of the content
     * @param items {object} containing two attributes:
     *  - href with the link,
     *  - text with the text of the link.
     */
    addNav(items)
    {
        let menu = this.querySelector('#menu-toggle ul');
        let lines = [];
        items.forEach((item) => {
            /*let link = document.createElement('a');
            link.setAttribute('href', item.href);
            // TODO(JBR) we should protect against injection here.
            link.innerHTML = item.text;*/
            let link = route.buildLink(item.href, item.text);
            let line = document.createElement('li');
            line.append(link);
            lines.push(line);
        });
        menu.innerHTML = '';
        lines.forEach(line => menu.appendChild(line));
    }

    /** Common to our custom elements **/

    static internalState = state.createStore();

    /** default attributes with default value. */
    static defaultAttributes = {
        'links-store': (given) => {
            if (given)
            {
                let navItems = state.store.getState(given);
                if (navItems)
                {
                    FdsToggleMenu.internalState.dispatch({
                        key: 'nav-items',
                        value: navItems
                    });
                }
                let sub = state.store.subscribe(
                    given,
                    (key, oldVal, newVal) => {
                        FdsToggleMenu.internalState.dispatch({
                            key: 'nav-items',
                            value: navItems
                        });
                    }
                );
                return sub;
            }
        }
    };

    // TODO(JBR) This method can move to mechanics when we start to have more
    // custom elements.
    async loadInnerHTML()
    {
        let remoteBaseDir = 'mechanics/custom-elements/fds-toggle-menu';
        let cssData = await fetchStaticAsString(`${remoteBaseDir}/style.css`);
        let htmlData = await fetchStaticAsString(`${remoteBaseDir}/index.html`);
        return `<style type="text/css" scoped>
    ${cssData}
</style>
${htmlData}`;
    }

    /**
     * In charge of element rendering as soon as the innerHTML is ready to
     * receive something (this will be designed to be overloaded).
     */
    render()
    {
        let navItems = FdsToggleMenu.internalState.getState('nav-items');
        if (navItems)
        {
            this.addNav(navItems);
        }
        else
        {
            this.subscriptions.push(FdsToggleMenu.internalState.subscribe(
                'nav-items',
                (key, oldVal, newVal) => {
                    if (newVal)
                    {
                        this.addNav(navItems);
                    }
                }
            ));
        }
    }

    /** Methods overriden from HTMLElement **/

    constructor()
    {
        super();
        // NB: Followings should be generic to all the custom elements.
        this.subscriptions = [];
        // TODO(JBR) Be careful, current version is shared accross instances of
        // the toggle menu (and maybe children). We have to use some uniq id
        // specific to the current instance. The store methods should be wrapped
        // in something to deal with the uniq ids added to the keys (as a possible
        // solution).
        // Additionally, it should be good to edit the register method so that
        // we can give default value to registered keys.
        FdsToggleMenu.internalState.register(['content-loaded', 'nav-items']);
        this.subscriptions.push(FdsToggleMenu.internalState.subscribe(
            'content-loaded',
            (key, oldVal, newVal) => {
                if (newVal)
                {
                    this.render();
                }
            }
        ));
        this.loadInnerHTML().then((data) => {
            this.innerHTML = data;
            FdsToggleMenu.internalState.dispatch({
                key: 'content-loaded',
                value: true
            });
        });
    }

    /**
     * Apply changes whenever the attribute attr is updated.
     * @param attr {String} the attribute which has changed.
     * @param oldValue {String} the old value.
     * @param newValue {String} the new value.
     */
    attributeChangedCallback(attr, oldValue, newValue)
    {
        this.subscriptions.push(FdsToggleMenu.defaultAttributes[attr](newValue));
    }

    /**
     * Method called whenevere the custom element is instanciated.
     * Here we connect the attributes of the current instance to the content.
     */
    connectedCallback()
    {
        Object.keys(FdsToggleMenu.defaultAttributes).forEach((k) => {
            let jsAttr = FdsToggleMenu.defaultAttributes[k];
            let htmlAttr = this.getAttribute(k);
            if (typeof jsAttr === 'function' && htmlAttr)
            {
                jsAttr(htmlAttr);
            }
        });
    }

    /**
     * Method called whenever the current instance is destroyed.
     */
    disconnectedCallBack() {
        this.storeSubscription.unsubscribe();
        this.subscriptions.forEach((sub) => { if (sub) sub.unsubscribe(); });
    }

    static get observedAttributes() {
        return Object.keys(FdsToggleMenu.defaultAttributes);
    }
}

export { FdsToggleMenu };
