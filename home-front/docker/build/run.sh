#!/bin/bash

# TODO(JBR) Think of a config file when we have more options to give to webpack.
echo "Setup $PWD/$2 js and css subdirectories."
mkdir -p $2/js/ $2/css
if [ $? -ne 0 ]; then exit 1; fi
echo "Building Javascript..."
webpack --entry $1/js/main.js --output-path $2/js/main.js
if [ $? -ne 0 ]; then exit 1; fi
echo "Building CSS..."
cp $1/css/*.css $2/css/*.css
if [ $? -ne 0 ]; then exit 1; fi
echo "Building HTML..."
cp $1/index.html $2/index.html
if [ $? -ne 0 ]; then exit 1; fi
