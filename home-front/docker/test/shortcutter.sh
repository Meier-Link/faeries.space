#! /bin/bash
# @file shortcutter.sh
# Just a little tool to generate shortcuts to node scripts

if [ $# -lt 2 ] || [ $# -gt 3 ]
then
    echo "Invalid use of shortcutter"
    exit 1
fi

cat > /usr/bin/$1 <<EOF
#! /bin/bash

/usr/bin/node $3 /node_modules/$2 \$@
EOF

chmod +x /usr/bin/$1
